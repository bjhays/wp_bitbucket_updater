CREDIT
--------------------
Refactoring of https://github.com/jkudish/WordPress-GitHub-Plugin-Updater to use bitbucket instead of github.


USAGE
--------------------
////////******************* updater *******************************/////
include_once('updater.php');
////////******************* updater *******************************/////

if (is_admin()) { // double check that this is happening in the admin
	$repo=""; // format: 'username/repo

    $config = array(
    	'uname' => '',
    	'upass' => '',
    	'repo' => $repo,
        'slug' => plugin_basename(__FILE__), // this is the slug of your plugin
        'proper_folder_name' => '', // this is the name of the folder your plugin lives in
        'version_url' => 'https://bitbucket.org/api/1.0/repositories/'.$repo.'/', // the raw url of your github repo
        'zip_url' => 'https://bitbucket.org/'.$repo.'/get/master.zip', // the zip url of the git repo
        'sslverify' => true, // wether WP should check the validity of the SSL cert when getting an update
        'requires' => '3.0', // which version of WordPress does your plugin require?
        'tested' => '3.3', // which version of WordPress is your plugin tested up to?
        'readme' => 'README.md', // which file to use as the readme for the version number
        'update_hrs' => '6', // how often to check for updates
    );
    new WP_BitBucket_Updater($config);
}


-------------------
To update your plugin from bitbucket commit the file README.md with "~Current Version:#~" in it. 
-------------------