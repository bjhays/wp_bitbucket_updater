<?php

// Prevent loading this file directly and/or if the class is already defined
if (class_exists( 'WP_BitBucket_Updater' ) ){
	return;
}

class WP_BitBucket_Updater {

	/**
	 * Updater version
	 */
	const VERSION = 1.0;

	const DEBUG=false;
	const WP_GIT_FORCE_UPDATE=false;

	/**
	 * @var $config the config for the updater
	 * @access public
	 */
	var $config;

	/**
	 * @var $missing_config any config that is missing from the initialization of this instance
	 * @access public
	 */
	var $missing_config;

	/**
	 * @var $git_data temporiraly store the data fetched from GitHub, allows us to only load the data once per class instance
	 * @access private
	 */
	private $git_data;


	/**
	 * Class Constructor
	 *
	 * @since 1.0
	 * @param array $config the configuration required for the updater to work
	 * @see has_minimum_config()
	 * @return void
	 */
	public function __construct( $config = array() ) {

		$defaults = array(
			'slug' => plugin_basename( __FILE__ ),
			'proper_folder_name' => dirname( plugin_basename( __FILE__ ) ),
			'sslverify' => true,
			'access_token' => '',
		);
		$this->config = wp_parse_args( $config, $defaults );

		$this->set_defaults();

		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'api_check' ) );

		// Hook into the plugin details screen
		add_filter( 'plugins_api', array( $this, 'get_plugin_info' ), 10, 3 );
		add_filter( 'upgrader_post_install', array( $this, 'upgrader_post_install' ), 10, 3 );

		// set timeout
		add_filter( 'http_request_timeout', array( $this, 'http_request_timeout' ) );

		// set sslverify for zip download
		add_filter( 'http_request_args', array( $this, 'http_request_sslverify' ), 10, 2 );
	}

	/**
	 * Set defaults
	 *
	 * @since 1.2
	 * @return void
	 */
	public function set_defaults() {
		if ( !empty( $this->config['access_token'] ) ) {

			// See Downloading a zipball (private repo) https://help.github.com/articles/downloading-files-from-the-command-line
			extract( parse_url( $this->config['zip_url'] ) ); // $scheme, $host, $path

			$zip_url = $scheme . '://api.github.com/repos' . $path;
			$zip_url = add_query_arg( array( 'access_token' => $this->config['access_token'] ), $zip_url );

			$this->config['zip_url'] = $zip_url;
		}


		if ( ! isset( $this->config['new_version'] ) )
			$this->config['new_version'] = $this->get_new_version();

		if ( ! isset( $this->config['last_updated'] ) )
			$this->config['last_updated'] = $this->get_date();

		if ( ! isset( $this->config['description'] ) )
			$this->config['description'] = $this->get_description();

		$plugin_data = $this->get_plugin_data();
		if ( ! isset( $this->config['plugin_name'] ) )
			$this->config['plugin_name'] = $plugin_data['Name'];

		if ( ! isset( $this->config['version'] ) )
			$this->config['version'] = $plugin_data['Version'];

		if ( ! isset( $this->config['author'] ) )
			$this->config['author'] = $plugin_data['Author'];

		if ( ! isset( $this->config['homepage'] ) )
			$this->config['homepage'] = $plugin_data['PluginURI'];

		if ( ! isset( $this->config['readme'] ) )
			$this->config['readme'] = 'README.md';

	}


	/**
	 * Callback fn for the http_request_timeout filter
	 *
	 * @since 1.0
	 * @return int timeout value
	 */
	public function http_request_timeout() {
		return 2;
	}

	/**
	 * Check wether or not the transients need to be overruled and API needs to be called for every single page load
	 *
	 * @return bool overrule or not
	 */
	public function overrule_transients() {
		return ( self::WP_GIT_FORCE_UPDATE);
	}

	/**
	 * Callback fn for the http_request_args filter
	 *
	 * @param unknown $args
	 * @param unknown $url
	 *
	 * @return mixed
	 */
	public function http_request_sslverify( $args, $url ) {
		if ( $this->config[ 'zip_url' ] == $url ){
			$args['headers'] = array(
					'Authorization' => 'Basic ' . base64_encode( $this->config['uname'] . ':' . $this->config['upass'] )
					);

			$args[ 'sslverify' ] = $this->config[ 'sslverify' ];
		}
		return $args;
	}

	private function curlUrl($url){
		$process = curl_init($url);
		curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', $additionalHeaders));
		curl_setopt($process, CURLOPT_HEADER, 0);
		curl_setopt($process, CURLOPT_USERPWD, $this->config['uname'] . ":" . $this->config['upass']);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
		$json = curl_exec($process);
		$curlResponse=curl_getinfo($process);
		curl_close($process);
		if($json===false || $curlResponse['http_code']!=200){
			throw new Exception("Curl Error: <pre>".print_r($curlResponse,true)."</pre>", 1);	
			return false;
		}
		return array("body"=>$json,"response"=>$curlResponse);
	}


	public function get_readme(){
		/////// get most current revision ////
		$revisionUrl="https://bitbucket.org/api/2.0/repositories/".$this->config['repo']."/commits/master";
		$curl=$this->curlUrl($revisionUrl);

		if($curl===false){
			return false;
		}
		$json=json_decode($curl['body']);
		unset($curl);
		$current_revision=$json->values[0]->hash;
		if(self::DEBUG){ echo "<br>Revision Url:$revisionUrl<br>Results:<pre>".print_r($json->values[0]->hash,true)."</pre>"; }
		//echo __CLASS__.'::'.__FUNCTION__.'('.__LINE__.') - current revision: '.$current_revision.'<br>';
		//2do save revision and only check the version if the revision has changed

		/////// get readme file to check version
		$versionUrl=$this->config['version_url'].'raw/'.$current_revision.'/'.$this->config['readme'];
		$curlVersion=$this->curlUrl($versionUrl);
		return $curlVersion['body'];
	}



	/**
	 * Get New Version from github
	 *
	 * @since 1.0
	 * @return int $version the version number
	 */
	public function get_new_version() {
		$version = get_site_transient( $this->config['slug'].'_new_version' );

		if ( $this->overrule_transients() || ( !isset( $version ) || !$version || '' == $version ) ) {

			$query = trailingslashit( $this->config['version_url'] ) . basename( $this->config['slug'] );
			$query = add_query_arg( array( 'access_token' => $this->config['access_token'] ), $query );

			/////// get most current revision ////
			$revisionUrl="https://bitbucket.org/api/2.0/repositories/".$this->config['repo']."/commits/master";
			$curl=$this->curlUrl($revisionUrl);

			if($curl===false){
				return false;
			}
			$json=json_decode($curl['body']);
			unset($curl);
			$current_revision=$json->values[0]->hash;
			if(self::DEBUG){ echo "<br>Revision Url:$revisionUrl<br>Results:<pre>".print_r($json->values[0]->hash,true)."</pre>"; }
			//echo __CLASS__.'::'.__FUNCTION__.'('.__LINE__.') - current revision: '.$current_revision.'<br>';
			//2do save revision and only check the version if the revision has changed

			/////// get readme file to check version
			$versionUrl=$this->config['version_url'].'raw/'.$current_revision.'/'.$this->config['readme'];
			$curlVersion=$this->curlUrl($versionUrl);
			
			//if response returns false or error
			if($curlVersion===false){
				return false;
			}

			preg_match('#^\\s*`*~Current Version\\:\\s*([^~]*)~#im', trim((string)$curlVersion['body']), $__version );
			if(self::DEBUG){ echo "<br>Version Url:$versionUrl<br>Results:<pre>".print_r($__version,true)."</pre>"; }

			if(empty($__version)){
				throw new Exception('Error fetching: '.$versionUrl.'<pre>'.print_r($curlVersion,true).print_r($__version,true).'</pre>', 1);
			}

			if ( isset( $__version[1] ) ) {
				$version_readme = $__version[1];
				if ( -1 == version_compare( $version, $version_readme ) )
					$version = $version_readme;
			}
			if ( false !== $version ){
				if(self::DEBUG){ echo $this->config['proper_folder_name']."  setting trasient: $version"; }
				set_site_transient( $this->config['proper_folder_name'].'_new_version', $version, 60*60*($this->config['update_hrs']) );
			}
		}
		if(self::DEBUG){ echo "returning version: $version"; }
		return $version;
	}


	/**
	 * Get GitHub Data from the specified repository
	 *
	 * @since 1.0
	 * @return array $git_data the data
	 */
	public function get_git_data() {
		if ( isset( $this->git_data ) && ! empty( $this->git_data ) ) {
			$git_data = $this->git_data;
		} else {
			$git_data = get_site_transient( $this->config['slug'].'_git_data' );

			if ( $this->overrule_transients() || ( ! isset( $git_data ) || ! $git_data || '' == $git_data ) ) {
				
				$url="https://bitbucket.org/api/1.0/repositories/".$this->config['repo'];
				$git_data=$this->curlUrl($url);

				$git_data = json_decode( $git_data['body'] );

				// refresh every 6 hours
				set_site_transient( $this->config['slug'].'_git_data', $git_data, 60*60*6 );
			}

			// Store the data in this class instance for future calls
			$this->git_data = $git_data;
		}

		return $git_data;
	}


	/**
	 * Get update date
	 *
	 * @since 1.0
	 * @return string $date the date
	 */
	public function get_date() {
		$_date = $this->get_git_data();
		return ( !empty( $_date->utc_last_updated ) ) ? date( 'Y-m-d', strtotime( $_date->utc_last_updated ) ) : false;
	}


	/**
	 * Get plugin description
	 *
	 * @since 1.0
	 * @return string $description the description
	 */
	public function get_description() {
		$_description = $this->get_git_data();
		return ( !empty( $_description->description ) ) ? $_description->description : false;
	}


	/**
	 * Get Plugin data
	 *
	 * @since 1.0
	 * @return object $data the data
	 */
	public function get_plugin_data() {
		include_once ABSPATH.'/wp-admin/includes/plugin.php';
		$data = get_plugin_data( WP_PLUGIN_DIR.'/'.$this->config['slug'] );
		return $data;
	}


	/**
	 * Hook into the plugin update check and connect to git
	 *
	 * @since 1.0
	 * @param object  $transient the plugin data transient
	 * @return object $transient updated plugin data transient
	 */
	public function api_check( $transient ) {

		// Check if the transient contains the 'checked' information
		// If not, just return its value without hacking it
		if ( empty( $transient->checked ) )
			return $transient;

		// check the version and decide if it's new
		$update = version_compare( $this->config['new_version'], $this->config['version'] );
		
		if ( 1 === $update ) {
			$response = new stdClass;
			$response->new_version = $this->config['new_version'];
			$response->slug = $this->config['proper_folder_name'];
			$response->package = $this->config['zip_url'];

			// If response is false, don't alter the transient
			if ( false !== $response ){
				$transient->response[ $this->config['slug'] ] = $response;
			}
		}

		return $transient;
	}


	/**
	 * Get Plugin info
	 *
	 * @since 1.0
	 * @param bool    $false  always false
	 * @param string  $action the API function being performed
	 * @param object  $args   plugin arguments
	 * @return object $response the plugin info
	 */
	public function get_plugin_info( $false, $action, $response ) {
		// Check if this call API is for the right plugin
		if ( $response->slug != $this->config['proper_folder_name'] ){
			return false;
		}

		$response->slug = $this->config['slug'];
		$response->plugin_name  = $this->config['plugin_name'];
		$response->version = $this->config['new_version'];
		$response->author = $this->config['author'];
		$response->homepage = $this->config['homepage'];
		$response->requires = $this->config['requires'];
		$response->tested = $this->config['tested'];
		$response->downloaded   = 0;
		$response->last_updated = $this->config['last_updated'];
		$response->sections = array( "Version Info"=>"<pre>".print_r($this->get_readme(),true)."</pre>" ,
									 'description' => $this->config['description'], 
									 "raw"=>"<pre>".print_r($this->get_git_data(),true)."</pre>", 
									);
		$response->download_link = $this->config['zip_url'];
		return $response;
	}


	/**
	 * Upgrader/Updater
	 * Move & activate the plugin, echo the update message
	 *
	 * @since 1.0
	 * @param boolean $true       always true
	 * @param mixed   $hook_extra not used
	 * @param array   $result     the result of the move
	 * @return array $result the result of the move
	 */
	public function upgrader_post_install( $true, $hook_extra, $result ) {

		global $wp_filesystem;
		// Move & Activate
		$proper_destination = WP_PLUGIN_DIR.'/'.$this->config['proper_folder_name'];
		$wp_filesystem->move( $result['destination'], $proper_destination );
		$result['destination'] = $proper_destination;
		$activate = activate_plugin( WP_PLUGIN_DIR.'/'.$this->config['slug'] );
		@unlink($result['destination']);

		// Output the update message
		$fail  = __( 'The plugin has been updated, but could not be reactivated. Please reactivate it manually.', 'git_plugin_updater' );
		$success = __( 'Plugin reactivated successfully.', 'github_plugin_updater' );
		echo is_wp_error( $activate ) ? $fail : $success;
		return $result;

	}
}

//}
